﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using Busuness.Objects;

namespace Data.Objects
{
    public class EmployeeRepository : RepositoryBase, IEmployeeRepository
    {
        public IEnumerable<Employee> GetEmployeesByNamePart(string namePart)
        {
            using (var connection = OpenConnection())
            {
                var employees = new List<Employee>();
                
                var querySelect =
                                 @"SELECT 
                                        [UserName]
                                        ,[LastName]
		                                FROM [vwEmployeeRoomPlace] erp
		                                    INNER JOIN [vwEmployees] e
		                                        ON erp.EmployeeID = e.EmployeeID
		                                    INNER JOIN [vwRoomsPlace] p
		                                        ON erp.PlaceID=p.PlaceID
		                                    INNER JOIN [vwRooms] r
		                                        ON p.RoomID=r.RoomID
		                                    INNER JOIN [vwOffices] o
		                                        ON r.OfficeID = o.OfficeID
		                                    INNER JOIN [vwRoomsType] t
		                                        ON r.RoomTypeID = t.RoomTypeID
                                            INNER JOIN [vwEmployeePhoto] ph
											    ON e.EmployeeID=ph.EmployeeID
		                                    WHERE o.City = 'Sofia' 
                                                AND t.Name = 'Work Room'  
                                                AND (DateTo > GETDATE() OR DateTo is null)
                                                AND LastName like '%'+@namePart+'%'
		                                    ORDER BY LastName";

                using (var command = new SqlCommand(querySelect, (SqlConnection)connection))
                {
                    command.Parameters.AddWithValue("@namePart", namePart);

                    using (var reader = command.ExecuteReader())
                    {

                        while (reader.Read())
                        {

                            employees.Add(
                                new Employee
                                {
                                    Name = Convert.ToString(reader["LastName"]),
                                    UniqueAccountName = Convert.ToString(reader["UserName"])
                                }
                            );
                        }
                    }
                }

                return employees;
            }
        }

        public Employee GetEmployee(string uniqueAccountName)
        {
            using (var connection = OpenConnection())
            {
                Employee employee = null;
                var querySelect =
                                @"SELECT 
                                    [UserName]
                                    ,[LastName]
                                    ,[PlaceNo]
                                    ,r.RoomName as Sector
	                                ,ph.Photo as Photo
		                          FROM 
                                    [vwEmployeeRoomPlace] erp
		                            INNER JOIN [vwEmployees] e
		                                ON erp.EmployeeID = e.EmployeeID
		                            INNER JOIN [vwRoomsPlace] p
		                                ON erp.PlaceID = p.PlaceID
		                            INNER JOIN [vwRooms] r
		                                ON p.RoomID = r.RoomID
		                            INNER JOIN [vwOffices] o
		                                ON r.OfficeID = o.OfficeID
		                            INNER JOIN [vwRoomsType] t
		                                ON r.RoomTypeID = t.RoomTypeID
                                    INNER JOIN [vwEmployeePhoto] ph
							            ON e.EmployeeID = ph.EmployeeID
		                            WHERE o.City = 'Sofia' 
                                        AND t.Name = 'Work Room'  
                                        AND (DateTo > GETDATE() OR DateTo is null)
                                        AND UserName = @uniqueAccountName";

                using (var command = new SqlCommand(querySelect, (SqlConnection)connection))
                {
                    command.Parameters.AddWithValue("@uniqueAccountName", uniqueAccountName);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var imageAsBytesObject = reader["Photo"];
                            var imageAsByteArray = (byte[])imageAsBytesObject;
                            string base64String = Convert.ToBase64String(imageAsByteArray);

                            employee = new Employee
                            {
                                Name = Convert.ToString(reader["LastName"]),
                                WorkplaceId = Int32.Parse(reader["PlaceNo"].ToString()),
                                Sector = Int32.Parse(reader["Sector"].ToString()),
                                Photo = $"data:image/gif;base64,{base64String}"
                            };
                        }
                    }
                }

                return employee;
            }
        }

        public Employee GetEmployeeByWorkplace(int placeId)
        {
            using (var connection = OpenConnection())
            {
                Employee employee = null;
                var querySelect =
                                @"SELECT 
                                    [UserName]
                                    ,[LastName]
                                    ,[PlaceNo]
                                    ,r.RoomName as Sector
	                                ,ph.Photo as Photo
		                          FROM 
                                    [vwEmployeeRoomPlace] erp
		                            INNER JOIN [vwEmployees] e
		                                ON erp.EmployeeID = e.EmployeeID
		                            INNER JOIN [vwRoomsPlace] p
		                                ON erp.PlaceID = p.PlaceID
		                            INNER JOIN [vwRooms] r
		                                ON p.RoomID = r.RoomID
		                            INNER JOIN [vwOffices] o
		                                ON r.OfficeID = o.OfficeID
		                            INNER JOIN [vwRoomsType] t
		                                ON r.RoomTypeID = t.RoomTypeID
                                    INNER JOIN [vwEmployeePhoto] ph
							            ON e.EmployeeID = ph.EmployeeID
		                            WHERE o.City = 'Sofia' 
                                        AND t.Name = 'Work Room'  
                                        AND (DateTo > GETDATE() OR DateTo is null)
                                        AND PlaceNo = @placeId";

                using (var command = new SqlCommand(querySelect, (SqlConnection)connection))
                {
                    command.Parameters.AddWithValue("@placeId", placeId);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var imageAsBytesObject = reader["Photo"];
                            var imageAsByteArray = (byte[])imageAsBytesObject;
                            string base64String = Convert.ToBase64String(imageAsByteArray);

                            employee = new Employee
                            {
                                Name = Convert.ToString(reader["LastName"]),
                                WorkplaceId = Int32.Parse(reader["PlaceNo"].ToString()),
                                Sector = Int32.Parse(reader["Sector"].ToString()),
                                Photo = $"data:image/gif;base64,{base64String}"
                            };
                        }
                    }
                }

                return employee;
            }
        }
    }
}
