﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Data.Objects
{
    public class RepositoryBase 
    {
        
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["SSEConnection"].ToString();
        public IDbConnection OpenConnection()
        {
            IDbConnection connection = new SqlConnection(connectionString);
            connection.Open();

            return connection;
        }
    }
}
