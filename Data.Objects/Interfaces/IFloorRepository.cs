﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Busuness.Objects;

namespace Data.Objects
{
    public interface IFloorRepository
    {
        IEnumerable<Floor> GetFloorsWithInhabitants();
        Floor GetFloor(int floor);
        Floor GetFloorBySector(int sectorId);
        IEnumerable<Team> GetTeamsByFloor(int floor);
    }
}