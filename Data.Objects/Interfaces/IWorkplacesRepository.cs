﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Busuness.Objects;

namespace Data.Objects
{
    public interface IWorkplacesRepository
    {
        Workplace GetWorkplace(int placeId);
        Workplace GetWorkplaceByEmployee(Employee employee);
        IEnumerable<Workplace> GetWorkplaces (int sectorId);

    }
}
