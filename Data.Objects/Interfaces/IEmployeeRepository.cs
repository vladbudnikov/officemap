﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Busuness.Objects;

namespace Data.Objects
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetEmployeesByNamePart(string id);
        Employee GetEmployee(string uniqueAccountName);
        Employee GetEmployeeByWorkplace(int placeId);
    }
}
