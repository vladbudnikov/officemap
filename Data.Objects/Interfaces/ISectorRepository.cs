﻿using System.Collections.Generic;
using Busuness.Objects;

namespace Data.Objects
{
    public interface ISectorRepository
    {
        Sector GetSector(int sectorId);
    }
}
