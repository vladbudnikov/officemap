﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Busuness.Objects;

namespace Data.Objects
{
    public class FloorRepository : RepositoryBase, IFloorRepository
    {
        public IEnumerable<Floor> GetFloorsWithInhabitants()
        {
            var floors = new List<Floor>();
            using (var connection = OpenConnection())
            {
                var selectFloor =   @"SELECT 
                                        FloorName, 
                                        COUNT(rp.PlaceNo) as EmployeesNumber
                                      FROM [SOFTSERVE].[dbo].[vwRooms] r
                                      INNER JOIN [SOFTSERVE].[dbo].[vwOffices] o
                                        ON o.OfficeID = r.OfficeID
                                      INNER JOIN [SOFTSERVE].[dbo].[vwRoomsType] rt
                                        ON r.RoomTypeID = rt.RoomTypeID
									  INNER JOIN vwRoomsPlace rp
									    ON rp.RoomID = r.RoomID
									  INNER JOIN [vwEmployeeRoomPlace] erp
									    ON rp.PlaceID = erp.PlaceID
									  INNER JOIN[vwEmployees] e
									    ON erp.EmployeeID = e.EmployeeID									  
                                      WHERE city = 'Sofia' AND rt.Name = 'Work Room' AND erp.DateTo is null
									  GROUP BY FloorName";

                using (var command = new SqlCommand(selectFloor, (SqlConnection)connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            floors.Add(new Floor
                            {
                                //Name = Convert.ToString(reader["FloorName"]),
                                Number = Convert.ToInt32(reader["FloorName"].ToString()),
                                Inhabitans = Int32.Parse(reader["EmployeesNumber"].ToString())
                            });
                        }
                    }
                }
                


                return floors;
            }
        }

        public IEnumerable<Team> GetTeamsByFloor(int floor)
        {
            var teams = new List<Team>();
            using (var connection = OpenConnection())
            {
                var selectFloorTeams = @"SELECT 
                                    distinct(orgname), 
                                    r.FloorName
                                FROM [vwEmployees] e
                                    INNER JOIN[vwOrgStructure] s 
                                        ON e.orgId = s.OrgID
                                    INNER JOIN [vwEmployeeRoomPlace] erp 
                                         ON erp.EmployeeID = e.EmployeeID
                                    INNER JOIN [vwRoomsPlace] rp 
                                         ON rp.PlaceID = erp.PlaceID
                                    INNER JOIN [vwRooms] r 
                                         ON rp.RoomID = r.RoomID
                                    INNER JOIN [vwOffices] o 
                                         ON o.OfficeID = r.OfficeID
								    INNER JOIN [vwPersonStatuses] ps 
                                         ON e.PersonStatusId = ps.Id
                                WHERE o.OfficeName = 'Sofia'
                                    AND ps.Name IN ('Billable',
                                                      'Intern',
                                                      'Locked Reserve',
                                                      'Mobile Reserve',
                                                      '(blank)')
                                    AND (erp.DateTo is null OR erp.DateTo > GETDATE())
                                    AND r.FloorName = @floor
                                GROUP BY orgname, r.FloorName";

                using (var command = new SqlCommand(selectFloorTeams, (SqlConnection)connection))
                {
                    command.Parameters.AddWithValue("@floor", floor.ToString());

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                                teams.Add(new Team { Name = Convert.ToString(reader["orgname"]) });
                        }
                    }
                }

                return teams;
            }
        }

        public Floor GetFloor(int floorId)
        {
            var fl = new Floor { Id = floorId, Name = floorId.ToString() };
            var sectors = new List<Sector>();
            using (var connection = OpenConnection())
            {
                var querySelect = @"SELECT [RoomName]                                      
                                              FROM [vwRooms] r
                                              INNER JOIN [vwOffices] o
                                                ON o.OfficeID = r.OfficeID
                                              INNER JOIN [vwRoomsType] rt
                                                ON r.RoomTypeID = rt.RoomTypeID
                                              WHERE city = 'Sofia' AND rt.Name = 'Work Room' AND FloorName=@floor";

                using (var command = new SqlCommand(querySelect, (SqlConnection)connection))
                {
                    command.Parameters.AddWithValue("@floor", floorId.ToString());

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            sectors.Add(new Sector
                            {
                                Name = Convert.ToString(reader["RoomName"]),
                                Number = Int32.Parse(reader["RoomName"].ToString()),
                                FloorId = floorId
                            });
                        }
                        fl.Sectors = sectors;
                    }
                }
                return fl;
            }
        }

        public Floor GetFloorBySector(int sectorId)
        {
            var fl = new Floor();
            var sectors = new List<Sector>();
            using (var connection = OpenConnection())
            {
                var querySelect = @"SELECT [RoomName]                                      
                                              FROM [vwRooms] r
                                              INNER JOIN [vwOffices] o
                                                ON o.OfficeID = r.OfficeID
                                              INNER JOIN [vwRoomsType] rt
                                                ON r.RoomTypeID = rt.RoomTypeID
                                              WHERE city = 'Sofia' AND rt.Name = 'Work Room' AND FloorName='2'";

                using (var command = new SqlCommand(querySelect, (SqlConnection)connection))
                {
                    //command.Parameters.AddWithValue("@surveyVersionId", surveyVersionId);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            sectors.Add(new Sector { Name = Convert.ToString(reader["RoomName"]) });
                        }
                        fl.Sectors = sectors;
                    }
                }
                return fl;
            }
        }
    }
}
