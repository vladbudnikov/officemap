﻿namespace Data.Objects
{
    public static class DbManager
    {
        public static IEmployeeRepository GetEmployeeRepository()
        {
            return new EmployeeRepository();
        }
        //public static IWorkplacesRepository GetWorkplaceRepository()
        //{
        //    return new WorkplacesRepository();
        //}
        public static ISectorRepository GetSectorRepository()
        {
            return new SectorRepository();
        }
        public static IFloorRepository GetFloorRepository()
        {
            return new FloorRepository();
        }
    }
}
