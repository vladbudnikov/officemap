﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Busuness.Objects;

namespace Data.Objects
{
    public class SectorRepository : RepositoryBase, ISectorRepository
    {
        public Sector GetSector(int sectorNumber)
        {
            using (var connection = OpenConnection())
            {
                var sector = new Sector {Name = sectorNumber.ToString(), Number = sectorNumber};

                var querySelectSector =
                                 @"SELECT rp.placeno as PlaceNo,
                                           floorname, 
                                           roomname, 
                                           e.lastname,
                                           e.UserName as userName 
                                FROM [vwrooms] r 
                                        JOIN vwoffices o 
                                            ON o.officeid = r.officeid 
                                        JOIN vwroomstype rt 
                                            ON rt.roomtypeid = r.roomtypeid 
                                        JOIN vwroomsplace rp 
                                            ON rp.roomid = r.roomid 
                                        LEFT JOIN vwemployeeroomplace erp 
                                            ON erp.placeid = rp.placeid 
                                        LEFT JOIN vwemployees e 
                                            ON e.employeeid = erp.employeeid 
                                WHERE o.officename LIKE 'Sofia' 
                                        AND rt.NAME = 'Work Room' 
                                        AND ( erp.dateto IS NULL 
                                                OR erp.dateto > Getdate() ) 
                                        AND RoomName = @sectorNumber
                                ORDER BY lastname ";

                using (var command = new SqlCommand(querySelectSector, (SqlConnection)connection))
                {
                    command.Parameters.AddWithValue("@sectorNumber", sectorNumber.ToString());

                    using (var reader = command.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            var s = Int32.Parse(reader["PlaceNo"].ToString());
                            sector.Workplaces.Add(new Workplace
                            {
                                PlaceAbsoluteNumber = Int32.Parse(reader["PlaceNo"].ToString()),
                                Employee = new Employee
                                {
                                    Name = Convert.ToString(reader["LastName"]),
                                    UniqueAccountName = Convert.ToString(reader["UserName"])
                                }
                            });
                        }
                    }
                }

                return sector;
            }
        }
    }
}
