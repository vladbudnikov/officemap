﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sofia.Office.Map.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Section",                                                                              // Route name
                "Floor/{action}/{floorNumber}/{sectorNumber}",                                                   // URL with parameters
                new { controller = "Floor", action = "DeepDetails", sector = UrlParameter.Optional },   // Parameter defaults
                constraints: new { floorNumber = @"\d+" , sectorNumber = @"\d+" } 
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
