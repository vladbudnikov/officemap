﻿(function () {

    var $input = $('.typeahead');

    $input.typeahead({
        source: function (query, process) {
            $.ajax({
                url: '/Employee/Index',
                type: 'POST',
                data: 'id=' + query,
                dataType: "JSON",
                async: true,
                success: function (data) {
                    console.log(data);
                    var resultList = data.map(function (item) {
                        var aItem = { key: item.UniqueAccountName, value: item.Name };
                        return JSON.stringify(aItem);
                    });
                    return process(resultList);
                }
            });
        },
        highlighter: function (item) {
            return JSON.parse(item).value;
        },
        matcher: function (item) {
            return JSON.parse(item).value.toLocaleLowerCase()
                .indexOf(this.query.toLocaleLowerCase()) !== -1;
        },
        updater: function (item) {
            $('#srch-term2').val(JSON.parse(item).key);
            return JSON.parse(item).value;
        }
    });
})();