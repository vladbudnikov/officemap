﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Busuness.Objects;
using Sofia.Office.Map.Web.Models;
using Web.Models;

namespace Sofia.Office.Map.Web.Mapping
{
    internal static class Mapper
    {
        internal static EmployeeViewModel Map(Employee businessObject)
        {
            return new EmployeeViewModel
            {
                Name = businessObject.Name,
                Competence = businessObject.Competence,
                Email = businessObject.Email,
                PhoneNumber = businessObject.PhoneNumber,
                Photo = businessObject.Photo,
                Title = businessObject.Title,
                UniqueAccountName = businessObject.UniqueAccountName
            };
        }

        internal static IEnumerable<EmployeeViewModel> Map(IEnumerable<Employee> businessObjects)
        {
            return businessObjects.Select(item => Map(item)).ToList();
        }

        internal static FloorViewModel Map(Floor businessObjectFloor)
        {
            return new FloorViewModel
            {
                Name = businessObjectFloor.Name,
                Teams = businessObjectFloor.Teams,
                Sectors = businessObjectFloor.Sectors
            };
        }
    }
}