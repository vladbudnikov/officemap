﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Busuness.Objects;

namespace Sofia.Office.Map.Web.Models
{
    public class SectorViewModel
    {
        public SectorViewModel(Floor floor)
        {
            this.Floor = floor;
            this.SectorId = 0;
        }
        public SectorViewModel(Floor floor, int sectorId = 0, Employee employee = null)
        {
            this.Floor = floor;
            this.Employee = employee;
            this.SectorId = sectorId;
        }
        public Floor Floor { get; set; }
        public int SectorId { get; set; }
        public Employee Employee { get; set; }
    }
}