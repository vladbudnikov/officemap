﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Busuness.Objects;

namespace Sofia.Office.Map.Web.Models
{
    public class FloorViewModel
    {
        public string Name { get; set; }
        public IEnumerable<Sector> Sectors { get; set; }
        public IEnumerable<Team> Teams { get; set; }
    }
}