﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Busuness.Objects;

namespace Sofia.Office.Map.Web.Models
{
    public class SectorEmployeeViewModel
    {
        public Sector Sector { get; set; }
        public Employee Employee { get; set; }
    }
}