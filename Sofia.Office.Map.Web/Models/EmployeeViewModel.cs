﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class EmployeeViewModel
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Competence { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public string UniqueAccountName { get; set; }
    }
}