﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Services;
using Business.Services.Interfaces;
using Busuness.Objects;

namespace Sofia.Office.Map.Web.Controllers
{
    public class SectorController : Controller
    {
        private readonly IEmployeeLocationService employeeLocationService = BusinessManager.GetEmployeeLocationService();
        
        //public ActionResult Index(string id)
        //{
        //    return View(employeeLocationService.GetWorkplaces(Convert.ToInt32(id)));
        //}

        public JsonResult Workplaces(string id)
        {
            var sector = employeeLocationService.GetSector(Int32.Parse(id));
            return Json(sector, JsonRequestBehavior.AllowGet);
        }

    }
}