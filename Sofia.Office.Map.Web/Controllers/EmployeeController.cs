﻿using System;
using System.Web.Mvc;
using Business.Services;
using Business.Services.Interfaces;
using Sofia.Office.Map.Web.Mapping;

namespace Sofia.Office.Map.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeLocationService employeeLocationService = BusinessManager.GetEmployeeLocationService();

        public ActionResult Index(string id)                                                        
        {
            var employees = employeeLocationService.GetEmployeesByNamePart(id);
            var emplyeeViewModels = Mapper.Map(employees);
            return Json(emplyeeViewModels, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(string id)                                                      
        {
            return PartialView(employeeLocationService.GetEmployee(id));

        }
        public ActionResult DetailsByPlaceId(string id)                                              
        {
            return PartialView("Details", employeeLocationService.GetEmployeeByWorkplace(Int32.Parse(id)));
        }
    }
}