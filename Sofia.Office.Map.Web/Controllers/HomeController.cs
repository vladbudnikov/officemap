﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Services;
using Business.Services.Interfaces;

namespace Sofia.Office.Map.Web.Controllers
{
    public class HomeController : Controller
    {

        private readonly IEmployeeLocationService employeeLocationService = BusinessManager.GetEmployeeLocationService();

        public ActionResult Index()                                                         
        {
            var floors = employeeLocationService.GetFloorsWithInhabitantsAndTeams();
            return View(floors);
        }
    }
}