﻿using System;
using System.Linq;
using System.Web.Mvc;
using Business.Services;
using Business.Services.Interfaces;
using Sofia.Office.Map.Web.Models;

namespace Sofia.Office.Map.Web.Controllers
{
    public class FloorController : Controller
    {
        private readonly IEmployeeLocationService employeeLocationService = BusinessManager.GetEmployeeLocationService();

        public ActionResult DeepDetails(int floorNumber, int? sectorNumber)                                
        {
            var floor = employeeLocationService.GetFloor(floorNumber);
            var sector = new SectorViewModel(floor);

            return View(sector);
        }

        public ActionResult Overview(int floorNumber)                                                         
        {
            var floor = employeeLocationService.GetFloor(floorNumber);
            var floorViewModel = Mapping.Mapper.Map(floor);
            return View(floorViewModel);
        }

        public ActionResult DeepDetailsWithSelectedEmployee(string id)                                  
        {
            var employee = employeeLocationService.GetEmployee(id);
            var floor = employeeLocationService.GetFloorBySector(employee.Sector);

            return View("DeepDetails", new SectorViewModel(floor, 0, employee));
        }                                                                                                  
    }
}