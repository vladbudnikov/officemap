﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Busuness.Objects;

namespace Business.Services.Interfaces
{
    public interface IEmployeeLocationService
    {
        #region Employee
        IEnumerable<Employee> GetEmployeesByNamePart(string namePart);
        Employee GetEmployee(string uniqueAccountName);
        Employee GetEmployeeByWorkplace(int parse);
        #endregion

        #region Floor
        IEnumerable<Floor> GetFloorsWithInhabitantsAndTeams();
        Floor GetFloor(int floor);
        Floor GetFloorBySector(int sectorId);
        #endregion

        #region Sector
        Sector GetSector(int sectorId);
        //IEnumerable<Sector> GetSectors(int floor);
        //Sector GetSectorByWorkplace(int workplaceId);
        #endregion

        //#region Workplaces
        ////Workplace GetWorkplace(int placeId);
        ////IEnumerable<Workplace> GetWorkplaces(int sectorId);
        ////Workplace GetWorkplaceByEmployee(Employee employee);
        //#endregion


    }
}