﻿using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using Business.Services.Interfaces;
using Busuness.Objects;
using Data.Objects;

namespace Business.Services
{
    public class EmployeeLocationService : IEmployeeLocationService
    {
        private readonly IEmployeeRepository employeeRepository = DbManager.GetEmployeeRepository();
        private readonly IFloorRepository floorRepository = DbManager.GetFloorRepository();
        private readonly ISectorRepository sectorRepository = DbManager.GetSectorRepository();
        //private readonly IWorkplacesRepository workplacesRepository = DbManager.GetWorkplaceRepository();

        public IEnumerable<Employee> GetEmployeesByNamePart(string namePart)
        {
            return employeeRepository.GetEmployeesByNamePart(namePart);
        }

        public Employee GetEmployee(string uniqueAccountName)
        {
            return employeeRepository.GetEmployee(uniqueAccountName);
        }

        public Employee GetEmployeeByWorkplace(int placeId)
        {
            return employeeRepository.GetEmployeeByWorkplace(placeId);
        }

        public IEnumerable<Floor> GetFloorsWithInhabitantsAndTeams()
        {
            var floors = floorRepository.GetFloorsWithInhabitants().ToList();

            foreach (var floor in floors)
            {
                floor.Name = Mapper.MapFloorNameByNumber(floor.Number);
                floor.Teams = floorRepository.GetTeamsByFloor(floor.Number);
            }

            return floors;
        }

        public Floor GetFloor(int floor)
        {
            return floorRepository.GetFloor(floor);
        }

        public Floor GetFloorBySector(int sectorId)
        {
            return floorRepository.GetFloorBySector(sectorId);
        }

        public Sector GetSector(int sectorId)
        {
            return sectorRepository.GetSector(sectorId);
        }
        //public Workplace GetWorkplace(int placeId)
        //{
        //    return workplacesRepository.GetWorkplace(placeId);
        //}

        //public Workplace GetWorkplaceByEmployee(Employee employee)
        //{
        //    return workplacesRepository.GetWorkplaceByEmployee(employee);
        //}

        //public IEnumerable<Workplace> GetWorkplaces(int sectorId)
        //{
        //    return workplacesRepository.GetWorkplaces(1);
        //}
    }
}