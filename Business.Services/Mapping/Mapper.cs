﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
 internal  static class Mapper
    {
        internal static string MapFloorNameByNumber(int floorNumber)
        {
            switch (floorNumber)
            {
                case 1: return "1st";
                case 2: return "2nd";
                case 3: return "3nd";
                case 4: return "4th";
                case 5: return "5th";
                case 6: return "6th";
                case 7: return "7th";
                case 8: return "8th";
                case 9: return "9th";
                case 10: return "10th";
                default:
                    return null;
            }
        }
    }
}
