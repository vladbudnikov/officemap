﻿using Business.Services.Interfaces;

namespace Business.Services
{
    public static class BusinessManager
    {
        public static IEmployeeLocationService GetEmployeeLocationService()
        {
            return new EmployeeLocationService();
        }
    }
}
