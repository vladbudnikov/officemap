﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Busuness.Objects
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Competence { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public string UniqueAccountName { get; set; }
        public Workplace Workplace { get; set; }
        public int WorkplaceId { get; set; }
        public string UserName { get; set; }
        public int Sector { get; set; }
    }
}
