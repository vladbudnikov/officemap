﻿using System.Collections.Generic;

namespace Busuness.Objects
{
    public class Floor
    {
        public int Id { get; set; }
        public string Name{ get; set; }
        public int Number { get; set; }
        public int Inhabitans { get; set; }
        public IEnumerable<Sector> Sectors { get; set; }
        public IEnumerable<Team> Teams { get; set; }
        public int FloorAbsoluteNumber { get; set; }
    }
}
