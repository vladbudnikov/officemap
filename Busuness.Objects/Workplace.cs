﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Busuness.Objects
{
    public class Workplace
    {
        public int Id { get; set; }
        public int PlaceAbsoluteNumber { get; set; }
        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int SectorId { get; set; }
        public Sector Sector { get; set; }
    }
}
