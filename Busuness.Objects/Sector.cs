﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Busuness.Objects
{
    public class Sector
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public int MapId { get; set; }
        public int Number { get; set; }
        public int FloorId { get; set; }
        public Floor Floor { get; set; }
        public IEnumerable<Team> Teams { get; set; }
        public List<Workplace> Workplaces { get; set; } = new List<Workplace>();
        public int SectorAbsoluteNumber { get; set; }
    }
}
