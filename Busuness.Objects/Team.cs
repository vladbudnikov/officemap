﻿using System.Collections.Generic;

namespace Busuness.Objects
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Sector> Sectors { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
    }
}